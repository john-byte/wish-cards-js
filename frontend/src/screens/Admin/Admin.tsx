import React from "react";
import { Card } from "../../components/Card";
import { Label } from "../../components/Label";
import { Input } from "../../components/Input";
import { Button } from "../../components/Button";
import { Row } from "../../components/Grid";
import { WidthContainer } from "../../components/WidthContainer";
import { VertCenterer } from "../../components/VertCenterer";
import { BASE_URL } from "../../consts";

const Admin = () => {
    const [secret, setSecret] = React.useState('');
    const [pending, setPending] = React.useState(false);

    const rawQuery = window.location.search.slice(1);
    const query = new URLSearchParams(rawQuery);
    const action = query.get('action') || 'close-register';

    const doAction = async () => {
        setPending(true);

        try {
            await fetch(BASE_URL + `/api/${action}`, {
                method: "POST",
                headers: {
                    'content-type': 'application/json',
                },
                body: JSON.stringify({
                    secret,
                })
            })
        } catch (e) {
            // @ts-ignore
            console.error(e.message);
        } finally {
            setPending(false);
        }
    };

    return (
        <WidthContainer style={{ width: '400px' }}>
            <VertCenterer>
                <Card>
                    <Label>Секрет</Label>
                    <Input 
                        value={secret}
                        type='password'
                        onChange={(e) => setSecret(e.currentTarget.value)}
                    />
                    <div style={{ height: '24px' }} />
                    <Row style={{ justifyContent: 'flex-end' }}>
                        <Button
                            onClick={doAction}
                            disabled={!secret || pending}
                        >
                            {pending ? (
                                '...'
                            ) : (
                                action === 'close-register' ? 'Закрыть регистрацию' : 'Открыть регистрацию'
                            )}
                        </Button>
                    </Row>
                </Card>
            </VertCenterer>
        </WidthContainer>
    )

}

export default Admin;
