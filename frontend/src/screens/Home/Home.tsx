import React from "react";
import { WidthContainer } from "../../components/WidthContainer";
import Enter from "./Enter";
import Wait from "./Wait";
import Loader from "../../components/Loader";
import Booklet from "./Booklet";
import WishCards from "./WishCards";
import { BASE_URL } from "../../consts";

const Home = () => {
    const [loading, setIsLoading] = React.useState(true);
    const [entered, setEntered] = React.useState(false);
    const [booklet, setBooklet] = React.useState('');
    const [receivers, setReceivers] = React.useState<string[]>([]);

    const auth = async () => {
        const nickname = localStorage.getItem('nickname');
            const password = localStorage.getItem('password');
            
            if (nickname && password) {
                const resp = await fetch(BASE_URL + "/api/enter", {
                    method: "POST",
                    headers: {
                        'content-type': 'application/json',
                    },
                    body: JSON.stringify({
                        nickname,
                        password,
                    })
                });
                const body = await resp.json();

                if (body && body.enterSuccess) {
                    localStorage.setItem('nickname', nickname);
                    localStorage.setItem('password', password);
                    handleEnterSuccess(body.registrationOpened);
                }
            }
    };

    const loadBooklet = async () => {
        const nickname = localStorage.getItem('nickname');
        const password = localStorage.getItem('password');

        if (nickname && password) {
            const resp = await fetch(BASE_URL + "/api/booklet", {
                method: "GET",
                headers: {
                    'x-nickname': nickname,
                    'x-password': password,
                }
            });

            const body = await resp.text();
            if (body) {
                setBooklet(body);
            }
        }
    };

    const loadReceivers = async () => {
        const nickname = localStorage.getItem('nickname');
        const password = localStorage.getItem('password');

        if (nickname && password) {
            const resp = await fetch(BASE_URL + "/api/nonfilled-receivers", {
                method: "GET",
                headers: {
                    'x-nickname': nickname,
                    'x-password': password,
                }
            });

            const body = await resp.json();
            if (body && body.enterSuccess && body.receivers?.length) {
                setReceivers(body.receivers);
            }
        }
    };

    const refetchReceivers = async () => {
        setIsLoading(true);
        try {
            await loadBooklet();
            await loadReceivers();
        } catch (e) {
            // @ts-ignore
            console.error(e.message);
        } finally {
            setIsLoading(false);
        }
    }

    const handleEnterSuccess = async (_isReg: boolean) => {
        await refetchReceivers();
        setEntered(true);
    }

    React.useEffect(() => {
        (async () => {
            setIsLoading(true);

            await auth();
            await loadBooklet();
            await loadReceivers();
        })().catch(err => console.error(err))
            .finally(() => setIsLoading(false));
    }, []); 

    // TODO: handle non-registraion flow
    return (
        <WidthContainer style={{ width: '640px' }}>
            {loading ? <Loader /> : (
                        !entered ? (
                            <Enter onSuccess={handleEnterSuccess} />
                        ) : booklet ? (
                            <Booklet booklet={booklet} />
                        ) : receivers.length ? (
                            <WishCards 
                                receivers={receivers}
                                refetch={refetchReceivers}
                            />
                        ) : (
                            <Wait />
                        )
                )}
        </WidthContainer>
    )
}

export default Home;
 