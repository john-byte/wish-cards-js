import React from "react";

import { H2, P } from "../../components/Text";
import { Card } from "../../components/Card";
import { VertCenterer } from "../../components/VertCenterer";

const Wait = () => {
    return (
        <VertCenterer>
            <Card style={{ padding: '32px' }}>
                <H2 style={{ textAlign: 'center' }}>
                    Вы зарегистрированы 
                </H2>
                <div style={{ height: '16px' }} />
                <P style={{ textAlign: 'center' }}>
                    Дождитесь закрытия регистрации для написания пожеланий и впечатлений
                </P>
            </Card>
        </VertCenterer>
    )
}

export default Wait;
