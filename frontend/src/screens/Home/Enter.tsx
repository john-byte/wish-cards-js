import React from "react";
import { Card } from "../../components/Card";
import { Label } from "../../components/Label";
import { Input } from "../../components/Input";
import { Button } from "../../components/Button";
import { Row } from "../../components/Grid";
import { BASE_URL } from "../../consts";
import { VertCenterer } from "../../components/VertCenterer";

interface Props {
    onSuccess: (isRegistration: boolean) => Promise<any> | void;
}

const Enter = ({ onSuccess }: Props) => {
    const [nickname, setNickname] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [isEntering, setIsEntering] = React.useState(false);

    const handleSubmit = async () => {
        if (nickname.length && password.length) {
            try {
                setIsEntering(true);
                const resp = await fetch(BASE_URL + "/api/enter", {
                    method: "POST",
                    headers: {
                        'content-type': 'application/json',
                    },
                    body: JSON.stringify({
                        nickname,
                        password,
                    })
                });

                const body = await resp.json();
                if (body && body.enterSuccess) {
                    localStorage.setItem('nickname', nickname);
                    localStorage.setItem('password', password);
                    onSuccess(body.registrationOpened);
                }
            } catch (e) {
                // @ts-ignore
                console.error(e.message);
            } finally {
                setIsEntering(false);
            }
        }
    };

    return (
        <VertCenterer style={{ width: '400px', margin: '0 auto' }}>
            <Card>
                <div>
                    <Label>
                        Никнейм
                    </Label>
                    <div style={{ width: '12px' }} />
                    <Input 
                        value={nickname}
                        onChange={(e) => setNickname(e.currentTarget.value)}
                    />
                </div>

                <div style={{ height: '12px' }} />

                <div>
                    <Label>
                        Пароль
                    </Label>
                    <div style={{ width: '12px' }} />
                    <Input 
                        value={password}
                        type="password"
                        onChange={(e) => setPassword(e.currentTarget.value)}
                    />
                </div>

                <div style={{ height: '40px' }} />

                <Row style={{ justifyContent: 'flex-end' }}>
                    <Button
                        disabled={!nickname.length || !password.length || isEntering} 
                        onClick={handleSubmit}
                    >
                        {isEntering ? '...' : 'Войти'}
                    </Button>
                </Row>
            </Card>
        </VertCenterer>
    )
}

export default Enter;
