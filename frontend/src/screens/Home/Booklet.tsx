import React from "react";
import { Card } from "../../components/Card";
import { Row } from "../../components/Grid";
import { Button } from "../../components/Button";

interface Props {
    booklet: string;
}

const Booklet = ({ booklet }: Props) => {
    const cardRef = React.useRef<HTMLDivElement>(null);

    React.useLayoutEffect(() => {
        if (cardRef.current) {
            cardRef.current.innerHTML = booklet;
        }
    }, [booklet])

    return (
        <div style={{ marginTop: '64px' }}>
            <Button onClick={() => window.print()}>Скачать PDF</Button>
            <div style={{ height: '16px' }} />
            <Card ref={cardRef} />
        </div>
    )
};

export default Booklet;
