import React from "react";
import styled from "styled-components";
import { Card } from "../../components/Card";
import { Label } from "../../components/Label";
import { Button } from "../../components/Button";
import { Row, Col } from "../../components/Grid";
import { Modal } from "../../components/Modal";
import { Textarea } from "../../components/Textarea";
import { BASE_URL } from "../../consts";
import { H4, P } from "../../components/Text";

interface Props {
    receivers: string[];
    refetch: () => void;
}

const WishCard = styled(Card)`
    cursor: pointer;
    transform: translateY(0);
    transition: 0.3s;
    width: 100%;
    min-height: 200px;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    &:hover {
        transform: translateY(-10px);
    }
`;

const WishCards = ({ receivers, refetch }: Props) => {
    const [pending, setPending] = React.useState(false);
    const [selectedReceiver, setSelectedReceiver] = React.useState('');
    const handleClose = () => setSelectedReceiver('');
    const [wish, setWish] = React.useState('');

    const writeWishcard = async () => {
        const nickname = localStorage.getItem('nickname');
        const password = localStorage.getItem('password');

        if (nickname && password) {
            setPending(true); 

            try {
                const resp = await fetch(BASE_URL + "/api/write-card", {
                    method: "POST",
                    headers: {
                        'content-type': 'application/json',
                    },
                    body: JSON.stringify({
                        from: nickname,
                        to: selectedReceiver,
                        senderPassword: password,
                        content: wish,
                    })
                });

                const body = await resp.json();

                if (body && body.enterSuccess && body.writeSuccess) {
                    handleClose();
                    refetch();
                }
            } catch (e) {
                // @ts-ignore
                console.error(e.message);
            } finally {
                setPending(false);
            }
        }
    }

    return (
        <div style={{ marginTop: '64px' }}>
            <Row>
                {receivers.map((e) => {
                    return (
                        <Col style={{ width: 'calc(33.33% - 12px)', marginRight: '12px', marginTop: '12px' }}>
                            <WishCard 
                                key={e} 
                                onClick={() => setSelectedReceiver(e)}
                            >
                                <H4 style={{ textAlign: 'center' }}>Напишите пожелание/впечатление о</H4>
                                <P style={{ textAlign: 'center' }}>{e}</P>
                            </WishCard>
                        </Col>
                    )    
                })}
            </Row>

            {selectedReceiver ? (
                <Modal handleClose={handleClose}>
                    <div style={{ minWidth: '420px' }}>
                        <Label>Твое пожелание/впечатление о человеке</Label>
                        <Textarea 
                            value={wish}
                            onChange={(e) => setWish(e.currentTarget.value)}
                        />
                        <div style={{ height: '16px' }} />
                        <Row style={{ justifyContent: 'flex-end' }}>
                            <Button
                                disabled={!selectedReceiver || pending}
                                onClick={writeWishcard}
                            >
                                {pending ? '...' : 'Отправить'}
                            </Button>
                        </Row>
                    </div>
                </Modal>
            ) : null}
        </div>
    )
};

export default WishCards;
