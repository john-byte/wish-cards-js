import React from "react";

const Loader = () => {
    React.useLayoutEffect(() => {
        const origCursor = window.getComputedStyle(document.body).cursor;
        document.body.style.cursor = 'wait';
        return () => {
            document.body.style.cursor = origCursor;
        };
    }, []);
    
    return null;
}

export default Loader;
