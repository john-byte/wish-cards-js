import styled from "styled-components";

// #19297c => St Patricks Blue
// #585481 => Purple Navy
// #a1867f => Cinereous
// #c49bbb => Lilac
// #d1bce3 => Thistle

export const Button = styled.button`
    color: white;
    background-color: #171717;
    padding: 8px 12px;
    min-width: 110px;
    height: 40px;
    outline: none;
    border: none;
    border-radius: 8px;   
    cursor: pointer;
    box-shadow: 3px 5px 5px -4px #000000;

    position: relative;
    transition: 0.2s;
    transform: translateY(0);

    &:active {
        transform: translateY(4px);
    }
`;
