import styled from "styled-components";

export const H1 = styled.h1`
    font-size: 64px;
    line-height: 72px;
    font-weight: bold;
    color: black;
    word-wrap: wrap;
`;

export const H2 = styled.h2`
    font-size: 48px;
    line-height: 56px;
    font-weight: bold;
    color: black;
    word-wrap: wrap;
`;

export const H4 = styled.h4`
    font-size: 12px;
    line-height: 20px;
    font-weight: bold;
    color: black;
    word-wrap: wrap;
`;

export const P = styled.p`
    font-size: 16px;
    line-height: 24px;
    color: black;
    word-wrap: wrap;
`;

