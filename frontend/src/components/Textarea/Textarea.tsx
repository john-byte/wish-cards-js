import styled from "styled-components";

export const Textarea = styled.textarea`
    box-sizing: border-box;
    display: block;
    width: 100%;
    background-color: white;
    min-height: 200px;
    font-size: 16px;
    line-height: 32px;
    color: black;
    outline: none;
    border: 1px solid black;
    border-radius: 4px;
    padding-left: 8px;
    resize: none;
`;
