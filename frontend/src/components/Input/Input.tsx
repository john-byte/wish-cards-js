import styled from "styled-components";

export const Input = styled.input`
    display: block;
    box-sizing: border-box;
    width: 100%;
    background-color: white;
    height: 24px;
    font-size: 16px;
    line-height: 32px;
    color: black;
    outline: none;
    border: 1px solid black;
    border-radius: 4px;
    padding-left: 8px;
`;
