import styled from 'styled-components';

export const Card = styled.div`
    background-color: #ffe9a4;
    box-shadow: 3px 5px 5px -4px #000000;
    box-sizing: border-box;
    padding: 16px;
    border-radius: 8px;
`;

