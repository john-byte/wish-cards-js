import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";

const Wrapper = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    background: rgba(0, 0, 0, 0.4);

    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

export const Inner = styled.div`
    max-width: 80%;
    max-height: 80%;
    background-color: #ffe9a4;
    box-shadow: 3px 5px 5px -4px #000000;
    box-sizing: border-box;
    padding: 16px;
    border-radius: 8px;
`;

interface Props {
    handleClose: () => void;
    children?: React.ReactNode;
}

const Modal = ({ handleClose, children }: Props) => {
    const containerInitialized = React.useRef(false);
    const container = React.useRef<HTMLElement>(
        document.createElement('div')
    );
    containerInitialized.current = true;

    const modalRoot = React.useRef<HTMLElement>(null);
    React.useLayoutEffect(() => {
        let root = document.getElementById("modal-root");
        if (!root) {
            root = document.createElement('div');
            document.body.appendChild(root);
        }
        // @ts-ignore
        modalRoot.current = root;
        modalRoot.current.appendChild(container.current);
        return () => {
            // @ts-ignore
            modalRoot.current.removeChild(container.current);
        }
    }, []);

    const innerRef = React.useRef<HTMLDivElement>(null);
    React.useEffect(() => {
        const close = (e: Event) => {
            if (e.target !== innerRef.current && !innerRef.current?.contains(e.target as any)) {
                handleClose();
            }
        }

        window.addEventListener('click', close);
        return () => {
            window.removeEventListener('click', close);
        }
    }, [handleClose]);

    return (
        ReactDOM.createPortal(
            <Wrapper>
                <Inner ref={innerRef}>
                    {children}
                </Inner>
            </Wrapper>,
            container.current,
        )
    );
}

export default Modal;




