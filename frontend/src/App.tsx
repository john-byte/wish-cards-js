import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Admin } from "./screens/Admin";
import { Home } from "./screens/Home";

// Basic flow works!
// UX and layout issues fixed
function App() {
  return (
    <Router>
      <Switch>
        <Route 
          path="/admin" 
          // @ts-ignore
          render={() => {
            return <Admin />
          }} 
        />
        <Route 
          path="/" 
          // @ts-ignore
          render={() => {
            return <Home />
          }} 
        />
      </Switch>
    </Router>
  );
}

export default App;
