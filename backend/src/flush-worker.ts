import Mongo from "mongodb";
import { parentPort } from "worker_threads";
import Creds from "./creds.json";
import { FlushEvent, FlushEventType, FlushEventUserRegistered, FlushEventWishcardAdd, FlusherContext } from "./types";

const handleUserRegistered = async (ctx: FlusherContext, event: FlushEventUserRegistered) => {
    const collection = ctx.db.collection("participants");
    
    try {
        await collection.insertOne(event.payload);
    } catch (e) {
        // @ts-ignore
        console.error(`[FLUSHER] ${e?.message}`);
    }
}

const handleWishcardAdd = async (ctx: FlusherContext, event: FlushEventWishcardAdd) => {
    const collection = ctx.db.collection("wish-cards");
    
    try {
        await collection.insertOne(event.payload);
    } catch (e) {
        // @ts-ignore
        console.error(`[FLUSHER] ${e?.message}`);
    }
}

const init = async () => {
    try {
        const connection = await Mongo.connect(Creds.mongourl);
        const db = connection.db("wish-cards");

        parentPort?.on("message", async (msg) => {
            const eventType = msg?.event;
            const context: FlusherContext = {
                db,
            };

            if (msg && typeof eventType === "string") {
                switch (eventType) {
                    case FlushEventType.UserRegister:
                        handleUserRegistered(context, msg);
                        break;
                    case FlushEventType.WishcardAdd:
                        handleWishcardAdd(context, msg);
                        break;
                    default:
                        console.warn(`[FLUSHER] Unknown event ${eventType}`);
                        break;
                }
            } else {
                console.warn(`[FLUSHER] Invalid event type => ${eventType}`);
            }
        });

        console.log("[FLUSHER] Flush worker started!");
    } catch (e) {
        // @ts-ignore
        console.error(`[FLUSHER] ${e.message}`);
    }
}

init(); 