import Router from "koa-router";
import { Context } from "koa";
import { State, FlushEvent, FlushEventType, Wishcard } from './types';
import crypto from "crypto";

interface EnterReqBody {
    nickname: string;
    password: string;
}

interface EnterResBody {
    enterSuccess: boolean;
    wasRegistered: boolean;
    registrationOpened: boolean;
}

interface RegistrationSwitchReqBody {
    secret: string;
}

interface RegistrationSwitchResBody {
    success: boolean;
    wasRegisterOpened: boolean;
}

interface WriteCardReqBody {
    from: string;
    to: string;
    content: string;
    senderPassword: string;
}

interface WriteCardResBody {
    enterSuccess: boolean;
    writeSuccess: boolean;
}

interface GetNonFilledReceiversResBody {
    enterSuccess: boolean;
    receivers: string[];
}

const assertNotEmptyString = (ctx: Context, name: string, value: unknown) => {
    ctx.assert(
        typeof value === "string" && value.length, 
        400,
        `"${name}" must not be empty`
    );
}

const calcPassHash = (password: string) => {
    return crypto.createHash("sha256").update(password).digest("hex");
}

const router = () => {
    const router = new Router<State>({
        prefix: "/api"
    });

    // Works!
    router.post("/enter", async (ctx, next) => {
        const reqBody: EnterReqBody = ctx.request.body;
        const nickname = reqBody?.nickname;
        const password = reqBody?.password;
        // @ts-ignore
        assertNotEmptyString(ctx, "nickname", nickname);
        // @ts-ignore
        assertNotEmptyString(ctx, "password", password);

        const resBody: EnterResBody = {
            wasRegistered: false,
            registrationOpened: false,
            enterSuccess: false,
        };
        const storage = ctx.state.storage;
        
        const registered = storage.participants.has(nickname);
        const registrationOpened = storage.registrationOn;
        const reqPassHash = calcPassHash(password);
        const storedPassHash = storage.participants.get(nickname)?.passHash;

        resBody.wasRegistered = registered;
        resBody.registrationOpened = registrationOpened;
        
        const isRegistering = registrationOpened && !registered;
        resBody.enterSuccess = isRegistering || reqPassHash === storedPassHash;

        if (isRegistering) {
            const entry = {
                nickname,
                passHash: reqPassHash,
            };

            storage.participants.set(nickname, entry);

            const event: FlushEvent = {
                event: FlushEventType.UserRegister,
                payload: entry,
            };
            ctx.state.flushWorker.postMessage(event);
        }

        ctx.response.body = resBody;
        await next();
    });

    // Works!
    router.post("/open-register", async (ctx, next) => {
        const reqBody: RegistrationSwitchReqBody = ctx.request.body;
        const secret = reqBody?.secret;
        // @ts-ignore
        assertNotEmptyString(ctx, "secret", secret);

        const registerOpened = ctx.state.storage.registrationOn;
        const resBody: RegistrationSwitchResBody = {
            success: false,
            wasRegisterOpened: registerOpened,
        };
        if (!registerOpened && secret === ctx.state.adminSecret) {
            ctx.state.storage.registrationOn = true;
            resBody.success = true;
        }
        
        ctx.response.body = resBody;
        await next();
    });

    // Works!
    router.post("/close-register", async (ctx, next) => {
        const reqBody: RegistrationSwitchReqBody = ctx.request.body;
        const secret = reqBody?.secret;
        // @ts-ignore
        assertNotEmptyString(ctx, "secret", secret);

        const registerOpened = ctx.state.storage.registrationOn;
        const resBody: RegistrationSwitchResBody = {
            success: false,
            wasRegisterOpened: registerOpened,
        };
        if (registerOpened && secret === ctx.state.adminSecret) {
            ctx.state.storage.registrationOn = false;
            resBody.success = true;
        }
        
        ctx.response.body = resBody;
        await next();
    });

    // Works!
    router.post("/write-card", async (ctx, next) => {
        const reqBody: WriteCardReqBody = ctx.request.body;
        const from = reqBody?.from;
        const to = reqBody?.to;
        const senderPassword = reqBody?.senderPassword;
        const content = reqBody?.content;
        assertNotEmptyString(ctx, "from", from);
        assertNotEmptyString(ctx, "to", to);
        assertNotEmptyString(ctx, "senderPassword", senderPassword);
        assertNotEmptyString(ctx, "content", content);

        const resBody: WriteCardResBody = {
            enterSuccess: false,
            writeSuccess: false,
        }

        const storage = ctx.state.storage;
        const registrationClosed = !storage.registrationOn;
        const sender = storage.participants.get(from);
        const senderPassHash = calcPassHash(senderPassword);
        const enterSuccess = registrationClosed && !!sender && senderPassHash === sender.passHash;

        const receiver = storage.participants.get(to);
        const cardAlreadyExists = !!storage.wishcards.get(from)
            ?.get(to);
        const writeSuccess = enterSuccess && !!receiver && !cardAlreadyExists;

        if (writeSuccess) {
            const card = {
                from: sender.nickname,
                to: receiver.nickname,
                content,
            };

            let existingCardsFrom = storage.wishcards.get(from);
            if (!existingCardsFrom) {
                existingCardsFrom = storage.wishcards.set(from, new Map()).get(from);
            }
            const existingCardsTo = existingCardsFrom?.get(to);
            if (!existingCardsTo) {
                existingCardsFrom?.set(to, card);
            }

            const event: FlushEvent = {
                event: FlushEventType.WishcardAdd,
                payload: card,
            };
            const flushWorker = ctx.state.flushWorker;
            flushWorker.postMessage(event);
        }

        resBody.enterSuccess = enterSuccess;
        resBody.writeSuccess = writeSuccess;
        ctx.response.body = resBody;
        await next();
    });

    // Works!
    router.get("/nonfilled-receivers", async (ctx, next) => {
        const headers = ctx.headers;
        const nickname = headers["x-nickname"];
        const password = headers["x-password"];
        assertNotEmptyString(ctx, "nickname", nickname);
        assertNotEmptyString(ctx, "password", password);
        
        const resBody: GetNonFilledReceiversResBody = {
            enterSuccess: false,
            receivers: [],
        };

        const storage = ctx.state.storage;
        const registrationClosed = !storage.registrationOn;
        const user = storage.participants.get(nickname as string);
        const userPassHash = calcPassHash(password as string);
        const enterSuccess = registrationClosed && !!user && userPassHash === user.passHash;
        
        const receivers: string[] = [];
        if (enterSuccess) {
            const participants = storage.participants;
            const wishcards = storage.wishcards.get(user.nickname);
            
            for (const part of participants) {
                const user2Nickname = part[1].nickname;
                const isNotSame = user2Nickname !== user.nickname;
                const notVotedYet = !wishcards?.has(user2Nickname);

                if (isNotSame && notVotedYet) {
                    receivers.push(user2Nickname);
                }
            }
        }

        resBody.enterSuccess = enterSuccess;
        resBody.receivers = receivers;
        ctx.response.body = resBody;
        await next();
    });

    // Works!
    router.get("/booklet", async (ctx, next) => {
        const headers = ctx.headers;
        const nickname = headers["x-nickname"];
        const password = headers["x-password"];
        assertNotEmptyString(ctx, "nickname", nickname);
        assertNotEmptyString(ctx, "password", password);
        
        const storage = ctx.state.storage;
        const registrationClosed = !storage.registrationOn;
        const user = storage.participants.get(nickname as string);
        const userPassHash = calcPassHash(password as string);
        const enterSuccess = registrationClosed && !!user && userPassHash === user.passHash;

        const participants = storage.participants;
        const reqParticipantsCnt = participants.size - 1;
        const wishcards = storage.wishcards;
        const wishcardsFrom = wishcards.get(nickname as string);
        const wishcardsFromCnt = wishcardsFrom?.size;

        let resBody = "";
        if (enterSuccess && wishcardsFromCnt === reqParticipantsCnt) {
            const template = ctx.state.bookletTemplate;

            const cards: Wishcard[] = [];
            for (const sendToWishcard of wishcards) {
                for (const recToWishcard of sendToWishcard[1]) {
                    const card = recToWishcard[1];

                    if (card.to === nickname) {
                        cards.push(card);
                    }
                }
            }

            resBody = template({
                cards,
            });
        }

        ctx.response.body = resBody;
        ctx.set("content-type", "text/html");
        ctx.response.status = 200;
    });

    router.use(async (ctx) => {
        ctx.set("content-type", "application/json");
        ctx.response.status = 200;
    });

    return router.routes();
}

export default router;
