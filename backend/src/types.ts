import { Db } from "mongodb";
import { Worker } from "worker_threads";
import { TemplateDelegate } from "handlebars";

export interface State {
    storage: SessionStorage;
    flushWorker: Worker;
    adminSecret: string;
    bookletTemplate: TemplateDelegate<any>;
}

export type Nickname = string;
export interface Wishcard {
    from: Nickname;
    to: Nickname;
    content: string;
}

export interface SessionStorage {
    registrationOn: boolean;
    participants: Map<Nickname, {
        nickname: Nickname;
        passHash: string;
    }>;
    wishcards: Map<Nickname, Map<Nickname, Wishcard>>
}

export interface FlushEventUserRegistered {
    event: FlushEventType.UserRegister,
    payload: {
        nickname: string;
        passHash: string;
    },
}

export interface FlushEventWishcardAdd {
    event: FlushEventType.WishcardAdd,
    payload: Wishcard,
}

export enum FlushEventType {
    UserRegister = "userRegister",
    WishcardAdd = "wishcardAdd",
}

export type FlushEvent = FlushEventUserRegistered | FlushEventWishcardAdd;

export interface FlusherContext {
    db: Db;
}
