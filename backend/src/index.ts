import Koa from "koa";
import Bodyparser from "koa-bodyparser";
import Cors from "koa-cors";
import http from "http";
import { Worker } from "worker_threads";
import fs from "fs/promises";
import Handlebars from "handlebars";
import path from "path";
import { SessionStorage, State } from './types';
import Creds from "./creds.json";
import ApiRouter from "./api-routes";
import { restoreSession } from "./restore-session";

const getExtension = (path: string) => {
    if (!path.length) return undefined;
    let e = path.length - 1;

    while (e >= 0 && path[e] !== '.') {
        e--;
    }

    return path.slice(e + 1);
}

const main = async () => {
    const flushWorker = new Worker("./lib/flush-worker.js");
    const app = new Koa<State>();

    let storage: SessionStorage = {
        registrationOn: true,
        participants: new Map(),
        wishcards: new Map(),
    };

    const doRestoreSession = process.argv.includes("--restore-session");
    if (doRestoreSession) {
        storage = await restoreSession();
    }

    const bookletPath = path.join(
        __dirname, "..", "..", "resources",
        "templates", "booklet.html",
    );
    const rawBooklet = await fs.readFile(bookletPath);
    const bookletString = rawBooklet.toString("utf-8");
    const bookletTemplate = Handlebars.compile(bookletString);

    const frontendPath = path.join(
        __dirname, "..", "..", "resources",
        "frontend"
    );
    const staticFilesPath = path.join(
        frontendPath, "static"
    );
    const indexPath = path.join(
        frontendPath, "index.html"
    );
    
    app.use(Cors());
    app.use(Bodyparser());
    app.use(async (ctx, next) => {
        ctx.state.storage = storage;
        ctx.state.flushWorker = flushWorker;
        ctx.state.adminSecret = Creds.adminSecret;
        ctx.state.bookletTemplate = bookletTemplate;
        await next();
    });
    
    const staticRegex = "/static";
    app.use(async (ctx, next) => {
        if (!ctx.path.startsWith(staticRegex)) {
            await next();
            return;
        };

        const cleanPath = ctx.path.replace("/static", "");
        const filePath = path.join(staticFilesPath, cleanPath);
        const ext = getExtension(filePath);

        let contentType = 'text/plain';
        switch (ext) {
            case 'js':
                contentType = 'application/javascript';
                break;
            case 'css':
                contentType = 'text/css';
                break;
        }

        try {
            const file = await fs.readFile(filePath);
            ctx.response.body = file.toString("utf-8");
            ctx.set("content-type", contentType);
            ctx.response.status = 200;
        } catch (e) {
            ctx.response.body = `File ${ctx.path} not found`;
            ctx.set("content-type", "text/plain");
            ctx.response.status = 404;
        }
    });

    app.use(async (ctx, next) => {
        if (ctx.path.startsWith("/api")) {
            await next();
            return;
        }

        try {
            const file = await fs.readFile(indexPath);
            ctx.response.body = file.toString("utf-8");
            ctx.set("content-type", "text/html");
            ctx.response.status = 200;
        } catch (e) {
            ctx.response.body = `Index not found`;
            ctx.set("content-type", "text/plain");
            ctx.response.status = 404;
        }
    });

    app.use(ApiRouter());

    const server = http.createServer(app.callback());
    server.listen(8088, () => {
        console.log("server is listening on port 8088");
    })
}

main();
