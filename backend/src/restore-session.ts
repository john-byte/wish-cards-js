import Mongo from "mongodb";
import Creds from "./creds.json";
import { SessionStorage } from "./types";

export const restoreSession = async (): Promise<SessionStorage> => {
    const res: SessionStorage = {
        registrationOn: false,
        participants: new Map(),
        wishcards: new Map(),
    };

    const connection = await Mongo.connect(Creds.mongourl);
    const db = connection.db("wish-cards");

    // Restore participants
    const participants = db.collection("participants");
    const queriedParticipants = await participants.find().toArray();
    for (const part of queriedParticipants) {
        const nick = part["nickname"];
        const passHash = part["passHash"];

        res.participants.set(nick, {
            nickname: nick,
            passHash,
        });
    }

    // Restore wish cards
    const wishCards = db.collection("wish-cards");
    const queriedWishCards = await wishCards.find().toArray();
    for (const card of queriedWishCards) {
        const from = card["from"];
        const to = card["to"];
        const content = card["content"];

        if (!res.wishcards.has(from)) {
            res.wishcards.set(from, new Map());
        }

        res.wishcards.get(from)?.set(to, {
            from,
            to,
            content,
        });
    }

    return res;
}

